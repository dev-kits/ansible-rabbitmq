# ansible to create rabbitmq cluster

## Requirements

- ansible 1.9 +
- GuestOS: CentOS 7

## Role Variables
Example :
``` yaml
rabbitmq_version: "3.6.6"
rabbitmq_erlang_cookie: changmepls

rabbitmq_create_cluster: no
rabbitmq_cluster_master: localhost
rabbitmq_RAM_node_type: no

rabbitmq_plugins:
  - rabbitmq_management
  - rabbitmq_management_agent

rabbitmq_users:
  - user: admin
    password: admin
    vhost: /
    configure_priv: .*
    read_priv: .*
    write_priv: .*
    tags: administrator

rabbitmq_users_removed:
  - guest
```

## Instructions
Install rabbitmq cluster

1. edit `environments/dev/inventory` add your node.Example 

``` yaml
[all]
rabbitmqnode01 ansible_host=10.0.0.1
rabbitmqnode02 ansible_host=10.0.0.2
rabbitmqnode03 ansible_host=10.0.0.3

[lb]
rabbitmqnode01
```
2. edit `site.yml` setting your Roles and Variables.Example 

``` yaml
- hosts: all
  become: yes
  vars:
    - rabbitmq_create_cluster: true
    - rabbitmq_cluster_master: rabbitmqnode01
  roles:
    - common
    - rabbitmq
- hosts: lb
  become: yes
  roles:
    - haproxy
```
3. run ansible playbook

``` bash
ansible-playbook -i environments/dev/inventory site.yml
```

## Tasks
**DONE:**

- common role to setup OS
- install signle node rabbitMQ
- configure rabbitMQ user
- install rabbitMQ pulgin 
- startup rabbitMQ management UI
- install rabbitMQ cluster
- add haproxy roles

**TODO:**

- setting rabbitMQ node type (disk or RAM)
- integration haproxy into rabbitmq roles


## Referenced
- [在CentOS7上配置RabbitMQ 3.6.3集群与高可用](http://www.jianshu.com/p/3a625aacd9aa)
- [RabbitMQ分布式集群架构和高可用性](http://chyufly.github.io/blog/2016/04/10/rabbitmq-cluster/)
- [rabbitMQ doc](https://www.rabbitmq.com/documentation.html)
